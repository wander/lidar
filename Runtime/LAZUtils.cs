﻿using laszip.net;
using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Rendering;

namespace LAZNamespace
{
    class LAZUtils
    {

        internal struct PointCloudNativeArrays
        {
            [System.Runtime.InteropServices.StructLayout( System.Runtime.InteropServices.LayoutKind.Sequential )]
            internal struct Vertex
            {
                internal Vector3 pos;
                internal Color col;
            }

            internal NativeArray<Vertex> vertices;
            bool disposed;

            internal Vector3[] ToPositionsAndDispose()
            {
                var array = vertices.ToArray().Select( v => v.pos ).ToArray();
                Dispose();
                return array;
            }

            internal Mesh ToMeshPositionColor( bool markUploadedMeshNoLongerReadable )
            {
                Stopwatch st = new Stopwatch();
                st.Start();
                var flags = MeshUpdateFlags.DontValidateIndices | MeshUpdateFlags.DontNotifyMeshUsers | MeshUpdateFlags.DontResetBoneBounds | MeshUpdateFlags.DontRecalculateBounds;
                Mesh m = new Mesh();
                var layout = new[]
                {
                    new VertexAttributeDescriptor(VertexAttribute.Position, VertexAttributeFormat.Float32, 3),
                    new VertexAttributeDescriptor(VertexAttribute.Color, VertexAttributeFormat.Float32, 4),
                };
                m.SetVertexBufferParams( vertices.Length, layout );
                m.SetVertexBufferData( vertices, 0, 0, vertices.Length, 0, flags );
                int [] indices = new int [vertices.Length];
                for (int i = 0;i < indices.Length;i++) indices[i] = i;
                m.SetIndexBufferParams( indices.Length, IndexFormat.UInt32 );
                m.SetIndexBufferData( indices, 0, 0, indices.Length, flags );
                var submesh = new []
                {
                    new SubMeshDescriptor(0, indices.Length, MeshTopology.Points)
                };
                m.SetSubMeshes( submesh, flags );
                m.RecalculateBounds( flags );
                m.UploadMeshData( markUploadedMeshNoLongerReadable );
                st.Stop();
                //     UnityEngine.Debug.Log( nameof( ToMeshPositionNormalColor ) + " call time: " + st.Elapsed.TotalSeconds + " seconds" );
                return m;
            }

            //internal unsafe static PointCloudNativeArrays FromCache( string file )
            //{
            //    PointCloudNativeArrays arr = new PointCloudNativeArrays();
            //    arr.disposed = true;
            //    if (!File.Exists( file ))
            //        return arr;
            //    using var fs = new BinaryReader( File.Open( file, FileMode.Open ) );
            //    int numVertices = fs.ReadInt32();
            //    int numIndices  = fs.ReadInt32();
            //    arr.vertices = new NativeArray<Vertex>( numVertices, Allocator.Persistent, NativeArrayOptions.UninitializedMemory );
            //    void* pVertices = NativeArrayUnsafeUtility.GetUnsafePtr( arr.vertices );
            //    arr.disposed = false;
            //    var spanVertices = new Span<byte>( pVertices, sizeof( Vertex )*numVertices );
            //    byte [] byteVertices = ArrayPool<byte>.Shared.Rent( sizeof( Vertex )*numVertices );
            //    byte [] byteIndices  = ArrayPool<byte>.Shared.Rent( sizeof( int )*numIndices );
            //    try
            //    {
            //        fs.Read( byteVertices, 0, sizeof( Vertex )*numVertices );
            //        fs.Read( byteIndices, 0, sizeof( int )*numIndices );
            //        byteVertices.CopyTo( pVertices, sizeof( Vertex )*numVertices );
            //    }
            //    finally
            //    {
            //        ArrayPool<byte>.Shared.Return( byteVertices );
            //        ArrayPool<byte>.Shared.Return( byteIndices ); 
            //    }
            //    return arr;
            //}

            //internal unsafe void ToCache( string file )
            //{
            //    if (disposed || !vertices.IsCreated || vertices.Length < 1024)
            //        return;
            //    Directory.CreateDirectory( Path.GetDirectoryName( file ) );
            //    using (var fs = new BinaryWriter( File.Open( file, FileMode.CreateNew ) ))
            //    {
            //        fs.Write( vertices.Length );
            //        void* pVertices = NativeArrayUnsafeUtility.GetUnsafePtr( vertices );
            //        fs.Write( new ReadOnlySpan<byte>( pVertices, vertices.Length*sizeof( Vertex ) ) );
            //    }
            //}

            internal void Dispose()
            {
                if (!disposed)
                {
                    disposed = true;
                    var thisCpy = this;
                    Task.Run( () =>
                    {
                        if (thisCpy.vertices.IsCreated) thisCpy.vertices.Dispose();
                    } );
                }
            }
        }

        // Adds coroutine layer around loading laz file.
        internal class WaitForLazAsync : CustomYieldInstruction
        {
            Task<PointCloudNativeArrays> t;
            int? tskId;
            Action<int> tskEnd;
            string error;

            public override bool keepWaiting
            {
                get
                {
                    if (t.IsCompleted)
                    {
                        tskEnd?.Invoke( tskId.Value );
                        return false;
                    }
                    return true;
                }
            }

            internal bool Succesful => t.IsCompletedSuccessfully;
            internal PointCloudNativeArrays Arrays => t.Result;
            internal int? BgtTaskId => tskId;
            internal string Error
            {
                get
                {
                    System.Diagnostics.Debug.Assert( t != null && t.IsCompleted );
                    return error;
                }
            }
            

            internal WaitForLazAsync( string laz, bool recenter, bool swapYZ, bool tryCache, bool showWarnings, Func<int> tskStart = null, Action<int> _tskEnd = null )
            {
                t = new Task<PointCloudNativeArrays>( () =>
                {
                    if (tryCache)
                        throw new Exception( "Not implemented" );
                    //if (tryCache)
                    //{
                    //    var ext = Path.GetExtension( laz );
                    //    string cacheName = Cache.GetCacheName( laz );
                    //    var cacheArr = PointCloudNativeArrays.FromCache( cacheName );
                    //    if (cacheArr.vertices.Length > 0)
                    //        return cacheArr;
                    //}
                    error = CreateNativeArraysFromLaz( laz, recenter, swapYZ, out PointCloudNativeArrays arrays );
                    if (error != null)
                        throw new InvalidOperationException( error );
                    //if (tryCache)
                    //{
                    //    arr.ToCache( Cache.GetCacheName( laz ) );
                    //}
                    return arrays;
                } );
                tskId  = tskStart?.Invoke();
                tskEnd = _tskEnd;
                t.Start();
            }

            internal Mesh ToMesh( bool markNoLongerReadable )
            {
                System.Diagnostics.Debug.Assert( t != null && t.IsCompleted );
                return Arrays.ToMeshPositionColor( markNoLongerReadable );
            }

            internal Mesh ToMeshAndDisposeArrays( bool markNoLongerReadable )
            {
                System.Diagnostics.Debug.Assert( t != null && t.IsCompleted );
                Mesh m = Arrays.ToMeshPositionColor( markNoLongerReadable );
                Dispose();
                return m;
            }

            internal void Dispose()
            {
                Arrays.Dispose(); 
            }
        }

        static string CreateNativeArraysFromLaz( string laz, bool recenter, bool swapYZ, out PointCloudNativeArrays arrays )
        {
            arrays = new PointCloudNativeArrays();
            var lazReader = new laszip_dll();
            var compressed = true;
            if ( 0 != lazReader.laszip_open_reader( laz, ref compressed ) )
            {
                string err = lazReader.laszip_get_error();
                lazReader.laszip_close_reader();
                return err;
            }
            var numberOfPoints = lazReader.header.number_of_point_records;

            // Check some header values
            Vector3 min = new Vector3((float)lazReader.header.min_x, (float)lazReader.header.min_y, (float)lazReader.header.min_z );
            Vector3 max = new Vector3((float)lazReader.header.max_x, (float)lazReader.header.max_y, (float)lazReader.header.max_z );
            Vector3 center = (min + max) / 2;

            // Put vertices in a native array so that it can be stored faster to VertexBuffer.
            NativeArray<PointCloudNativeArrays.Vertex> vertices = new NativeArray<PointCloudNativeArrays.Vertex>( (int) numberOfPoints, Allocator.Persistent, NativeArrayOptions.UninitializedMemory );

            // Loop through number of points indicated
            //coordinates coordArray = new coordinates();
            double [] coordArray = new double[3];
            const float oneOver65535 = 1.0f / 65535;
            Vector3 offset = recenter ? center : Vector3.zero;
            for (int i = 0;i < numberOfPoints; i++)
            {
                // Read the point
                if ( 0 != lazReader.laszip_read_point() )
                {
                    string err = lazReader.laszip_get_error();
                    lazReader.laszip_close_reader();
                    return err;
                }

                // Compute coordinate
                //    lazReader.laszip_get_coordinates( ref coordArray );
                //    Vector3 point = new Vector3((float)coordArray.x, (float)coordArray.y, (float)coordArray.z);
                lazReader.laszip_get_coordinates( coordArray );
                Vector3 point = new Vector3((float)coordArray[0], (float)coordArray[1], (float)coordArray[2]);

                // Get color
                float r = lazReader.point.rgb[0]*oneOver65535;
                float g = lazReader.point.rgb[1]*oneOver65535;
                float b = lazReader.point.rgb[2]*oneOver65535;

                // Assemble vertex
                PointCloudNativeArrays.Vertex vertex = new PointCloudNativeArrays.Vertex();
                vertex.pos.x = point.x-offset.x;
                vertex.pos.y = point.z-offset.z;
                vertex.pos.z = point.y-offset.y;
                if ( swapYZ )
                {
                    float t = vertex.pos.y;
                    vertex.pos.y = vertex.pos.z;
                    vertex.pos.z = t;
                }
                vertex.col.r = r;
                vertex.col.g = g;
                vertex.col.b = b;
                vertex.col.a = 1;
                vertices[i] = vertex;
            }

            // Close the reader
            lazReader.laszip_close_reader(); 
            arrays.vertices = vertices;

            return null;
        }
    }
}