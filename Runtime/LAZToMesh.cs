using UnityEngine;
using System.Collections;
using UnityEditor;

namespace LAZNamespace
{
    [ExecuteInEditMode()]
    internal class LAZToMesh : MonoBehaviour    
    {
        [Tooltip("LIDAR file to load.")]
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public string filename;
        [Tooltip("Material to apply.")]
        public Material material;
        [Tooltip( "Bring loaded file to centre of world." )]
        public bool placeInCentre;
        [Tooltip( "Swap YZ coordinate." )]
        public bool swapYZ;

        private void Awake()
        {
#if UNITY_EDITOR
            if (material == null)
            {
                string path = AssetDatabase.GUIDToAssetPath("68bc4c0ada39be44aa98db8162ae8c43");
                if ( !string.IsNullOrEmpty(path))
                {
                    material = AssetDatabase.LoadAssetAtPath<Material>( path );
                }
            }
#endif
        }

        internal IEnumerator GenerateRoutine()
        {
#if UNITY_EDITOR
            var task = new LAZUtils.WaitForLazAsync( filename, placeInCentre, swapYZ, false, true );
            yield return task;
            if (task.Succesful)
            {
                Mesh m = task.ToMeshAndDisposeArrays( true );
                GameObject go = new GameObject("LAZ");
                go.AddComponent<MeshRenderer>().sharedMaterial = material;
                go.AddComponent<MeshFilter>().sharedMesh = m;
            }
            else
            {
                Debug.LogWarning( task.Error );
            }
#endif
            yield return null;
        }

        internal void Generate()
        {
            StartCoroutine( GenerateRoutine() );
        }
    }

#if UNITY_EDITOR
    [CustomEditor( typeof( LAZToMesh ) )]
    [InitializeOnLoad]
    public class LAZToMeshEditor : Editor
    {
        static LAZToMeshEditor()
        {
        }

        public override void OnInspectorGUI()
        {
            LAZToMesh lazToMesh = (LAZToMesh)target;

            GUILayout.BeginHorizontal();
            {
                if ( GUILayout.Button( "Select file" ) )
                {
                    string s = EditorUtility.OpenFilePanel( lazToMesh.filename, "", "" );
                    s = s.Trim();
                    if (!string.IsNullOrEmpty( s ))
                    {
                        lazToMesh.filename = s;
                    }
                    else
                    {
                        Debug.LogWarning( "Invalid filename" );
                    }
                }
            }
            GUILayout.EndHorizontal();

         //   EditorGUILayout.HelpBox( "File must be in StreamingAssets folder or subdirectory thereof.", MessageType.Warning );

            DrawDefaultInspector();

            Repaint();
        }
    }
#endif

}