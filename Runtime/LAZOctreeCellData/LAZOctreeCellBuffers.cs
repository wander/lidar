﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Wander;

namespace LAZNamespace
{
    internal class LAZOctreeCellBuffers : LAZOctreeCellGfxData
    {
        GraphicsBuffer gbPosition;
        GraphicsBuffer gbColor;
        LAZOctree tree;
        LAZOctreeCell cell;

        internal GraphicsBuffer PositionBuffer => gbPosition;
        internal GraphicsBuffer ColorBuffer => gbColor;

        //void LAZOctreeCellGfxData.CreateGfxDataStage1MT( LAZOctreeCell _cell, CachedGfxObjects cml, int nthCell )
        //{
        //    Debug.Assert( _cell == null );
        //    cell = _cell;
        //    tree = cell.Tree;

        //    if (cml == null)
        //    {
        //        // The vertex count per cell is not yet know at this stage.
        //    }
        //    else
        //    {
        //        if (cml.BufferColors == null || cml.BufferColors == null)
        //            throw new System.IO.InvalidDataException( "Cache invalid." );

        //        Vector4 [] positions = cml.BufferPositions[nthCell];
        //        Vector4 [] colors = cml.BufferColors[nthCell];

        //        if (positions == null || colors == null)
        //            throw new System.IO.InvalidDataException( "Cache invalid." );

        //        gbPosition = new GraphicsBuffer( GraphicsBuffer.Target.Structured, 0, 16 );
        //        gbColor = new GraphicsBuffer( GraphicsBuffer.Target.Structured, 0, 16 );
        //        gbPosition.SetData( positions );
        //        gbColor.SetData( colors );

        //        // TODO do this somehow async. Need this for collision.
        //        if (cell.vertices == null)
        //        {
        //            cell.vertices  = new List<LAZVertex>();
        //            for (int i = 0;i < positions.Length;i++)
        //            {
        //                LAZVertex v = new LAZVertex();
        //                v.pos = positions[i];
        //                cell.vertices.Add( v );
        //            }
        //        }
        //    }
        //}
        //void LAZOctreeCellGfxData.CreateGfxDataStage2MT()
        //{
        //    gbPosition = new GraphicsBuffer( GraphicsBuffer.Target.Structured, cell.vertices.Count, 16 );
        //    gbColor = new GraphicsBuffer( GraphicsBuffer.Target.Structured, cell.vertices.Count, 16 );
        //    const float oneOver655356 = 1.0f / 65536;
        //    gbPosition.SetData( cell.vertices.Select( v =>
        //    {
        //        return new Vector4( v.pos.x, v.pos.y, v.pos.z, v.classification<<8 );
        //    } ).ToList() );
        //    gbColor.SetData( cell.vertices.Select( v =>
        //    {
        //        return new Vector4( v.r*oneOver655356, v.g*oneOver655356, v.b*oneOver655356, v.intensity );
        //    } ).ToList() );
        //}

        //void LAZOctreeCellGfxData.CreateGfxDataStage2WT()
        //{
        //}

        //void LAZOctreeCellGfxData.CreateGfxDataStage3MT()
        //{

        //}

        //void LAZOctreeCellGfxData.ReleaseMT()
        //{
        //    if (gbPosition != null) gbPosition.Release();
        //    if (gbColor != null) gbColor.Release();
        //    gbPosition = null;
        //    gbColor = null;
        //    GC.SuppressFinalize( this );
        //}

        public void MarkDirty()
        {

        }

        void LAZOctreeCellGfxData.UnloadDynamic()
        {
            
        }
    }
}
