#define LAZ_USE_BUFFERS

using laszip.net;
using ProjNet.CoordinateSystems.Transformations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UnityEngine;
using Wander;
using static Wander.Projections;
using Debug = System.Diagnostics.Debug;


namespace LAZNamespace
{
    [System.Runtime.InteropServices.StructLayout( System.Runtime.InteropServices.LayoutKind.Sequential )]
    public struct LAZVertex // Must be multiple of 4.
    {
        public Vector3 position;
        public byte r, g, b;
        public byte classification; /* Put classification in short, to make access in shader easier) -> UNorm16, x4 */
        public float intensity; // must maintain multiple of 4
        /*internal ushort intensity; */

        public void Write(BinaryWriter bw)
        {
            bw.Write( position.x );
            bw.Write( position.y );
            bw.Write( position.z );
            bw.Write( r );
            bw.Write( g );
            bw.Write( b );
            bw.Write( classification );
            bw.Write( intensity );
        }

        public static void Read(BinaryReader br, ref LAZVertex v)
        {
            v.position.x = br.ReadSingle();
            v.position.y = br.ReadSingle();
            v.position.z = br.ReadSingle();
            v.r = br.ReadByte();
            v.g = br.ReadByte();
            v.b = br.ReadByte();
            v.classification = br.ReadByte();
            v.intensity = br.ReadSingle();
        }
    }

    public class LAZOctree
    {
        internal LAZLoader Loader => loader;
        internal float PercentageDone { get; private set; }
        internal bool IsLoadedFromCache { get; private set; }
        internal bool IsReady => PercentageDone==100;
        internal LAZOctreeCell Root => root;
        internal string Filename => filename;
        internal double CentreWorldX => centreWorldX; // World coord
        internal double CentreWorldY => centreWorldY; // ''
        internal double CentreWorldZ => centreWorldZ; // ''
        internal Vector3 MinLocal => minLocal; // Local coords
        internal Vector3 MaxLocal => maxLocal; // Local coords
        internal ulong NumPoints => numberOfPoints;
        internal ulong NumLoadedPoints => numberOfLoadedPoints;
        internal ulong NumPointsInVoxels => numberOfPointsInVoxels;
        internal int NumLodsPerCell => loader.numLODSPerCell;
        internal int NumPointsPerCell => loader.numPointsPerCell;
        internal CellDivideMethod DivideMethod => loader.divideMethod;
        internal laszip_dll LazReader => lazReader;
        internal string PathToCacheFolder { get; private set; }

        // stats
        internal int numLeafs;
        internal int numCells;
        internal int numIntermediateCells;
        internal int maxTreeDepth;
        internal float sizeGB;
        internal float compressRatio;
        internal Dictionary<byte, byte> classifications;

        string filename;
        string relativePath;
        string cacheFolder;
        LAZLoader loader;
        LAZOctreeCell root;
        laszip_dll lazReader;
        double centreWorldX, centreWorldY, centreWorldZ; // Places coordinates back in absolute space. Keep in doubles.
        Vector3 minLocal, maxLocal; // min-max in local coordinates (after subtracting centre).
        ulong numberOfPoints;
        ulong numberOfLoadedPoints;
        ulong numberOfPointsInVoxels;
        ICoordinateTransformation crs;

        internal LAZOctree( LAZLoader _loader, LAZRenderer renderer )
        {
            loader       = _loader;
            filename     = renderer.fileName;
            relativePath = renderer.relativePath;
            cacheFolder  = renderer.cacheFolder;
            PathToCacheFolder = Path.Combine( loader.AppDataPath, relativePath, cacheFolder ).NormalizePath();
            bool src = renderer.sourceProjectionWKT.HasContent();
            bool tar = loader.targetProjectionCustomWKT.HasContent();
            if ( src && tar )
            {
                crs = CreateProjection( renderer.sourceProjectionWKT, loader.targetProjectionCustomWKT );
            }
            else if ( !src && tar )
            {
                crs = CreateProjection( renderer.sourceProjection, loader.targetProjectionCustomWKT );
            }
            else if ( src && !tar )
            {
                crs = CreateProjection( renderer.sourceProjectionWKT, loader.targetProjection );
            }
            else crs = CreateProjection( renderer.sourceProjection, loader.targetProjection );
        }

        internal void Reset()
        {
            PercentageDone = 0;
            IsLoadedFromCache = false;
            if (lazReader != null)
            {
                lazReader.laszip_close_reader();
            }
        }

        internal bool AreSourceAndTargetCRSEqual()
        {
            // TODO more sophisticated
            if (crs == null) return true;// Results in no transformation applied.
            return crs.SourceCS.Name == crs.TargetCS.Name;
        }

        internal void SetAndValidateBounds( double minx, double miny, double minz, double maxx, double maxy, double maxz )
        {
            // Transform the min/max to correct coordinate system.
            ( double x, double y, double z) maxTransformed = (maxx, maxy, maxz);
            ( double x, double y, double z) minTransformed = (minx, miny, minz);
            if (crs != null && !AreSourceAndTargetCRSEqual())
            {
                maxTransformed = crs.MathTransform.Transform( maxx, maxy, maxz );
                minTransformed = crs.MathTransform.Transform( minx, miny, minz );
            }

            // Read centre and min/max. Note swap y/z
            {
                centreWorldX = (maxTransformed.x+minTransformed.x)*0.5;
                centreWorldY = (maxTransformed.z+minTransformed.z)*0.5;   // Note swap z/y
                centreWorldZ = (maxTransformed.y+minTransformed.y)*0.5;

                minLocal = new Vector3(
                    (float)(minTransformed.x-centreWorldX),
                    (float)(minTransformed.z-centreWorldY/*centre Z already swapped with Y*/),
                    (float)(minTransformed.y-centreWorldZ) );

                maxLocal = new Vector3(
                    (float)(maxTransformed.x-centreWorldX),
                    (float)(maxTransformed.z-centreWorldY/*centre Z already swapped with Y*/),
                    (float)(maxTransformed.y-centreWorldZ) );
            }

            if (!(MinLocal.IsSane() && MaxLocal.IsSane() && CentreWorldX.IsSane() && CentreWorldY.IsSane() && CentreWorldZ.IsSane()) ||
                 !Vector3.Distance( MinLocal, MaxLocal ).IsSane()  ||
                 (Vector3.Distance( MinLocal, MaxLocal ) == 0))
            {
                UnityEngine.Debug.LogWarning( "Invalid coordinates calculated, is the source and target projection correct?" );
            }
        }

        internal void BuildFromAscii()
        {
            Stopwatch sw = new Stopwatch();

            Debug.Assert( loader != null );
            Debug.Assert( !IsLoadedFromCache );

            // Read all lines
            sw.Restart();
            var loadName = Path.Combine( Path.Combine( loader.AppDataPath, relativePath ), filename ).NormalizePath();
            var lines = File.ReadAllLines( loadName);
            numberOfPoints = (ulong)lines.LongLength;
            sw.StopAndPrintTiming( "Read all lines (ascii)" );

            // Get all or percentage of points.
            sizeGB = (float)((24 * numberOfPoints) / 1073741824.0 /*to gb*/);
            compressRatio = sizeGB / loader.maxMemorySizeGB;
            compressRatio = Mathf.Max( 0, (compressRatio-1) );

            // Check if source and target crs are same, to avoid unecessary calcs
            bool areSourceAndTargetCRSEqual = AreSourceAndTargetCRSEqual();

            // Declare variables needed in each iteration.
            float compressAccum = 0;
            float twentyFiveOverNumPoints = 25.0f / numberOfPoints; // Consider loading points 25% of the work, relatively arbitrary.

            // Get min/max
            sw.Restart();
            var points = new List<(double, double, double)>();
            double minx = double.MaxValue;
            double miny = double.MaxValue;
            double minz = double.MaxValue;
            double maxx = double.MinValue;
            double maxy = double.MinValue;
            double maxz = double.MinValue;
            for (int i = 0;i <lines.Length;i++)
            {
                // skip point?
                if (compressAccum > 1)
                {
                    compressAccum -= 1;
                    continue;
                }

                // read & validate point
                string [] asciiPoint = lines[i].Split( ' ', StringSplitOptions.RemoveEmptyEntries );
                if (!(asciiPoint != null && asciiPoint.Length==3))
                    continue;

                double x = MiscUtils.ParseDouble( asciiPoint[0] );
                double y = MiscUtils.ParseDouble( asciiPoint[1] );
                double z = MiscUtils.ParseDouble( asciiPoint[2] );
                if (x < minx) minx = x;
                if (y < miny) miny = y;
                if (z < minz) minz = z;
                if (x > maxx) maxx = x;
                if (y > maxy) maxy = y;
                if (z > maxz) maxz = z;

                if (!areSourceAndTargetCRSEqual)
                {
                    crs.MathTransform.Transform( ref x, ref y, ref z );
                    Debug.Assert( x.IsSane() && y.IsSane() && z.IsSane() );
                }

                // Build the point/vertex.
                points.Add( new( x, z, y ) ); // Note: swap z/y
                numberOfLoadedPoints++;

                compressAccum += compressRatio;

                // Update loading stats
                PercentageDone += twentyFiveOverNumPoints;
                loader.PercentageCompleted = PercentageDone;
            }

            // Sets min/MaxLocal and centre
            SetAndValidateBounds( minx, miny, minz, maxx, maxy, maxz );

            sw.StopAndPrintTiming( "Reading points (ascii)" );

            // Build the tree
            root = new LAZOctreeCell( this );
            root.InitPhysicalParams( minLocal, maxLocal );
            root.vertices = new List<LAZVertex>();

            sw.Restart();
            LAZVertex vt = new LAZVertex();
            for ( int i = 0; i < points.Count; i++ )
            {
                vt.position.x = (float)(points[i].Item1/*X*/ - centreWorldX); // Both now have swapped z/w, so dont swap again!
                vt.position.y = (float)(points[i].Item2/*Y*/ - centreWorldY);
                vt.position.z = (float)(points[i].Item3/*Z*/ - centreWorldZ);
                // vt.classification = (3)<<8; Testing purpose
                root.AddVertex( vt );

                if (root.vertices.Count > NumPointsPerCell)
                {
                    root.SplitAndFlush( false ); // This splits vertices down, keeps splitting it until only NumPointsPerCell or less are in leafs.
                }
                
                // Update loading stats
                PercentageDone += twentyFiveOverNumPoints;
                loader.PercentageCompleted = PercentageDone;
            }
            sw.StopAndPrintTiming( "Building tree (ascii)" );
        }
        internal void ReadHeaderLaz()
        {
            Debug.Assert( loader != null );
            Debug.Assert( !IsLoadedFromCache );
            
            // Open the file and read initial information
            var loadName = Path.Combine( Path.Combine( loader.AppDataPath, relativePath ), filename ).NormalizePath();
            if (lazReader != null)
            {
                lazReader.laszip_close_reader();
            }
            lazReader = new laszip_dll();
            var compressed = true;
            if (0 != lazReader.laszip_open_reader( loadName, ref compressed ))
            {
                UnityEngine.Debug.LogWarning( lazReader.laszip_get_error() );
                return;
            }
            numberOfPoints = lazReader.header.number_of_point_records;
            if (numberOfPoints == 0)
            {
                numberOfPoints = lazReader.header.extended_number_of_point_records;
            }
            Debug.Assert( numberOfPoints > 0, "No points" );

            // Pff sometimes NO bounding box is defined.. need to run twice through the points..
            double minx = double.MaxValue;
            double miny = double.MaxValue;
            double minz = double.MaxValue;
            double maxx = double.MinValue;
            double maxy = double.MinValue;
            double maxz = double.MinValue;
            if (lazReader.header.min_x == 0 && lazReader.header.max_x == 0)
            {
                double [] coordArray = new double[3];
                for (ulong i = 0;i < numberOfPoints;i++)
                {
                    int err = lazReader.laszip_read_point();
                    lazReader.laszip_get_coordinates( coordArray );
                    if (coordArray[0] < minx) minx = coordArray[0];
                    if (coordArray[1] < miny) miny = coordArray[1];
                    if (coordArray[2] < minz) minz = coordArray[2];
                    if (coordArray[0] > maxx) maxx = coordArray[0];
                    if (coordArray[1] > maxy) maxy = coordArray[1];
                    if (coordArray[2] > maxz) maxz = coordArray[2];
                }
                lazReader.laszip_seek_point( 0 );
            }
            else
            {
                maxx = lazReader.header.max_x;
                maxy = lazReader.header.max_y;
                maxz = lazReader.header.max_z;
                minx = lazReader.header.min_x;
                miny = lazReader.header.min_y;
                minz = lazReader.header.min_z;
            }

            SetAndValidateBounds( minx, miny, minz, maxx, maxy, maxz );
        }
        internal void BuildLaz()
        {
            Debug.Assert( !IsLoadedFromCache );

            Stopwatch sw = new Stopwatch();
            root = new LAZOctreeCell( this );
            root.InitPhysicalParams( minLocal, maxLocal );
            root.vertices = new List<LAZVertex>();

            // Get all or percentage of points.
            sizeGB = (float) ((24 * numberOfPoints) / 1073741824.0 /*to gb*/);
            compressRatio = sizeGB / loader.maxMemorySizeGB;
            compressRatio = Mathf.Max( 0, (compressRatio-1) );
            classifications = new Dictionary<byte, byte>();

            // Check if source and target crs are same, to avoid unecessary calcs
            bool areSourceAndTargetCRSEqual = AreSourceAndTargetCRSEqual();

            // Declare variables needed in each iteration.
            LAZVertex vt = new LAZVertex();
            double [] coordArray = new double[3];
            float oneOver65535  = 1.0f / 65535;
            float compressAccum = 0;
            float fiftyOverNumPoints = 50.0f / numberOfPoints;
            byte cacheClassifKey   = 255;
            byte cacheclassifValue = 0;

            // Unpack all points and add to root.
            sw.Restart();
            for (ulong i = 0;i < numberOfPoints;i++)
            {
                if ( loader.IsStopping )
                {
                    lazReader.laszip_close_reader();
                    lazReader = null;
                    return;
                }

                int err = lazReader.laszip_read_point();
                if (err != 0)
                {
                    UnityEngine.Debug.LogWarning( lazReader.laszip_get_error() );
                    return;
                }

                if (compressAccum <= 1)
                {
                    lazReader.laszip_get_coordinates( coordArray );
                    numberOfLoadedPoints++;

                    if (!areSourceAndTargetCRSEqual)
                    {
                        crs.MathTransform.Transform( ref coordArray[0], ref coordArray[1], ref coordArray[2] );
                        Debug.Assert( coordArray[0].IsSane() && coordArray[1].IsSane() && coordArray[2].IsSane() );
                    }

                    // Build the point/vertex.
                    vt.position.x = (float)(coordArray[0]-centreWorldX);
                    vt.position.y = (float)(coordArray[2]-centreWorldY); // Note Swap z/y, centre is already swapped.
                    vt.position.z = (float)(coordArray[1]-centreWorldZ);
                    vt.r = (byte)(lazReader.point.rgb[0]>>8); // Colors stoed as non-normalized, shader will normalize
                    vt.g = (byte)(lazReader.point.rgb[1]>>8);
                    vt.b = (byte)(lazReader.point.rgb[2]>>8);
                    var classf = lazReader.point.classification; 
                    if (classf != cacheClassifKey)
                    {
                        cacheClassifKey = classf;
                        if (!classifications.TryGetValue( cacheClassifKey, out byte classfFromCache ))
                        {
                            cacheclassifValue = (byte)classifications.Count;
                            classifications.Add( cacheClassifKey, cacheclassifValue );
                        }
                        else cacheclassifValue = classfFromCache;
                    }
                    vt.intensity = 1-(LazReader.point.intensity*oneOver65535); // Stored in uv0.r in shader as normalized, shader wont normalize
                    vt.classification = cacheclassifValue;

                    root.AddVertex( vt );
                    
                    if (root.vertices.Count > NumPointsPerCell)
                    {
                        root.SplitAndFlush( false ); // This splits vertices down, keeps splitting it until only NumPointsPerCell or less are in leafs.
                    }
                    compressAccum += compressRatio;
                }
                else compressAccum -= 1;

                // Update loading stats
                PercentageDone += fiftyOverNumPoints;
                loader.PercentageCompleted = PercentageDone;
            }
            if (LazReader != null) // Can be null if was loaded from cache.
            {
                lazReader.laszip_close_reader();
                lazReader = null;
            }

            sw.StopAndPrintTiming( "Reading points" );
        }

        internal void FinalizeVoxelTree()
        {
            Stopwatch sw = new Stopwatch();
            sw.Restart();
            root.SplitAndFlush( true );
            root.RemoveEmptyCells();
            int leafIterator = 0;
            root.AssignVoxelIndex( ref leafIterator );
            sw.StopAndPrintTiming( "Flush and cleaning tree" );
        }

        internal void FinalizeAndGetStats()
        {
            Stopwatch sw = new Stopwatch();

            // Verify and finalize cells and leafs.
            sw.Restart();
            root.Verify();
            sw.StopAndPrintTiming( "Verify" );

            // Get some stats.
            sw.Restart();
            maxTreeDepth = 0;
            numLeafs = 0;
            numCells = 0;
            numIntermediateCells = 0;
            numberOfPointsInVoxels = 0;
            Traverse( (cell, treeDepth) =>
            {
                if (cell.IsLeaf)
                {
                    if ( cell.vertices != null )
                    {
                        numberOfPointsInVoxels += (ulong)cell.vertices.Count;
                    }
                    numLeafs++;
                }
                else
                {
                    numIntermediateCells++;
                }
                numCells++;
                maxTreeDepth = Math.Max( maxTreeDepth, treeDepth );
                return true;
            });
            sw.StopAndPrintTiming( "Counting stats" );

            PercentageDone = 100;
        }

        internal void ComputeLODPerCellPerRenderer(LAZRenderer renderer, Plane[] frustumPlanes, Vector3 camOrigin )
        {
            Matrix4x4 worldTransform;
            renderer.GetWorldTransform( out worldTransform );
            root?.ComputeAndSubscribePerCellInterest( renderer, frustumPlanes, camOrigin, ref worldTransform );
        }

        internal void TraceHull( LAZRenderer renderer, TraceHullQuery hq )
        {
            // Place hull points in lazTree local space, this saves many multiplications for puting the vertices in world space.
            Matrix4x4 worldTransform;
            renderer.GetWorldTransform( out worldTransform );
            Matrix4x4 invWorldTransform = worldTransform.inverse;
            for ( int i = 0; i < hq.points.Count; i++)
            {
                hq.points[i] = invWorldTransform.MultiplyPoint( hq.points[i] );
            }
            root.TraceHull( ref hq.indices, hq.points, hq.tris, hq.pointsToProgress, renderer );
        }

        internal void TraceVertices( LAZRenderer renderer, TraceVerticesQuery vq )
        {
            // Place sphere in lazTree local space, this saves many multiplications for puting the vertices in world space.
            Matrix4x4 worldTransform;
            renderer.GetWorldTransform( out worldTransform );
            Matrix4x4 invWorldTransform = worldTransform.inverse;
            var invOrigin  = invWorldTransform.MultiplyPoint( vq.centre );
            root.TraceVertices( ref vq.indices, invOrigin, vq.radius, vq.pointsToProgress, renderer );
        }

        internal void TraceClosestPoint( LAZRenderer renderer, TraceLineQuery q )
        {
            // Place ray in lazTree local space, this saves many multiplications for puting the vertices in world space.
            Matrix4x4 worldTransform;
            renderer.GetWorldTransform( out worldTransform );
            Matrix4x4 invWorldTransform = worldTransform.inverse;
            var invOrigin  = invWorldTransform.MultiplyPoint( q.start );
            var invDir     = invWorldTransform.MultiplyVector( q.dir );
            Ray invRay     = new Ray(invOrigin, invDir.normalized);
            q.pointSkipStep = Mathf.Max( 1, q.pointSkipStep );
            root.TraceClosestPoint( invRay, q, renderer );
        }

        internal void WriteTreeFile()
        {
            if (!loader.streamData)
                return;

            Stopwatch sw = new Stopwatch();
            sw.Restart();

            try
            {
                // Ensure this directory exists.
                if (!Directory.Exists( PathToCacheFolder ))
                {
                    Directory.CreateDirectory( PathToCacheFolder );
                }

                // Write tree
                var filePath = Path.Combine( PathToCacheFolder, Path.GetFileNameWithoutExtension( Filename ) + ".tree" ).NormalizePath();
                using (var file = File.Open( filePath, FileMode.Create, FileAccess.Write ))
                using (var br = new BinaryWriter( file ))
                {
                    br.Write( numberOfPoints );
                    br.Write( numberOfLoadedPoints );
                    br.Write( numberOfPointsInVoxels );
                    br.Write( classifications.Count );
                    br.Write( sizeGB );
                    br.Write( compressRatio );
                    br.Write( minLocal.x );
                    br.Write( minLocal.y );
                    br.Write( minLocal.z );
                    br.Write( maxLocal.x );
                    br.Write( maxLocal.y );
                    br.Write( maxLocal.z );
                    br.Write( centreWorldX );
                    br.Write( centreWorldY );
                    br.Write( centreWorldZ );
                    Traverse( (cell, td) =>
                    {
                        cell.Write( br );
                        return true;
                    });
                }

                // Write leaf/voxel data
                Traverse( (cell, td) =>
                {
                    if (cell.IsLeaf)
                    {
                        cell.WriteLeafData();
                        cell.LeafReady = true;
                    }
                    return true;
                } );
            }
            catch (Exception e)
            {
                UnityEngine.Debug.LogException( e );
            }

            sw.StopAndPrintTiming( "Write tree to disk" );
        }

        internal bool ReadTreeFile()
        {
            Stopwatch sw = new Stopwatch();
            sw.Restart();

            Debug.Assert( root == null );
            try
            {
                // Ensure this directory exists.
                if (!Directory.Exists( PathToCacheFolder ))
                {
                    return false;
                }

                var filePath = Path.Combine( PathToCacheFolder, Path.GetFileNameWithoutExtension( Filename ) + ".tree" ).NormalizePath();
                if (!File.Exists(filePath))
                {
                    return false;
                }

                // Read tree
                classifications = new Dictionary<byte, byte>();
                using (var file = File.Open( filePath, FileMode.Open, FileAccess.Read ))
                using (var br = new BinaryReader( file ))
                {
                    numberOfPoints = br.ReadUInt64();
                    numberOfLoadedPoints = br.ReadUInt64();
                    numberOfPointsInVoxels = br.ReadUInt64();
                    int numClassifications = br.ReadInt32();
                    for (int i = 0;i < numClassifications;i++)
                        classifications.Add( (byte)i, (byte)i );
                    sizeGB = br.ReadSingle();
                    compressRatio = br.ReadSingle();
                    minLocal.x = br.ReadSingle();
                    minLocal.y = br.ReadSingle();
                    minLocal.z = br.ReadSingle();
                    maxLocal.x = br.ReadSingle();
                    maxLocal.y = br.ReadSingle();
                    maxLocal.z = br.ReadSingle();
                    centreWorldX = br.ReadDouble();
                    centreWorldY = br.ReadDouble();
                    centreWorldZ = br.ReadDouble();
                    root = new LAZOctreeCell( this );
                    root.Read( br );
                }

                // NOTE: Reading the actual leaf data is done from the renderers (lazy loading). This makes the loading progressive!
                IsLoadedFromCache = true;
                sw.StopAndPrintTiming( "Read tree from disk" );
                return true;
            }
            catch (Exception e)
            {
                UnityEngine.Debug.LogException( e );
            }
            return false;
        }

        public void SaveAsCSV()
        {
            //loader.StartGenericTask( () =>
            {
                var csvVertices = new List<(float x, float y, float z, byte classfic)>((int)numberOfPoints);
                for (int i = 0; i < (int) numberOfPoints; i++)
                {
                    csvVertices.Add( (0, 0, 0, 0) );
                }
                Traverse( ( cell, td ) => // Find the point to preserve original order.
                {
                    if (cell.IsLeaf)
                    {
                        cell.TraverseVertices( ( List<LAZVertex> vertices ) =>
                        {
                            for (int i = 0; i < vertices.Count; i++)
                            {
                                var vtx = vertices[i];
                                (float x, float y, float z, byte classfic) csvVtx;
                                csvVtx.x = vtx.position.x + (float)centreWorldX;
                                csvVtx.y = vtx.position.y + (float)centreWorldY;
                                csvVtx.z = vtx.position.z + (float)centreWorldZ;
                                csvVtx.classfic = (byte)(vtx.classification);
                            }
                            return true;
                        } );
                    }
                    return true;
                } );

                var filename = Path.Combine( PathToCacheFolder, Path.GetFileNameWithoutExtension( Filename ) + "_ann" + ".txt" );
                using StreamWriter sw = File.CreateText( filename );
                for (int i = 0; i < csvVertices.Count;i++)
                {
                    float x = csvVertices[i].x;
                    float y = csvVertices[i].y;
                    float z = csvVertices[i].z;
                    byte classif = csvVertices[i].classfic;
                    x = (float)Math.Round( x, 3 );
                    y = (float)Math.Round( y, 3 );
                    z = (float)Math.Round( z, 3 );
                    sw.WriteLine( "{0:F3} {1:F3} {2:F3} {3}", x, z, y, classif );
                }


            }// );
        }

        internal void Traverse( Func<LAZOctreeCell, int, bool> cb )
        {
            int treeDepth = 0;
            root.Traverse( cb, treeDepth );
        }
    }
}